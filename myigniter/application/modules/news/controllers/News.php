<?php defined("BASEPATH") or exit("No direct script access allowed");

/**
 * News Controller.
 */
class News extends MY_Controller
{
    private $title;

    public function __construct()
    {
        parent::__construct();

        $this->title = "News";
    }

    /**
     * Index
     */
    public function index()
    {
    }

    /**
     * CRUD
     */
	public function crudNews()
	{
		$crud = new grocery_CRUD();
		
		$crud->set_table("news");
		$crud->set_subject("News");

		// Show in
		$crud->add_fields(["subTitle", "title", "caption", "content", "image", "date", "categoryId", "userId"]);
		$crud->edit_fields(["subTitle", "title", "caption", "content", "image", "date", "categoryId"]);
		$crud->columns(["subTitle", "title", "caption", "content", "image", "date", "categoryId", "userId"]);

		// Fields type
		$crud->field_type("newsId", "string");
		$crud->field_type("subTitle", "string");
		$crud->field_type("title", "string");
		$crud->field_type("caption", "string");
		$crud->field_type("content", "text");
		$crud->set_field_upload("image", 'assets/uploads');
		$crud->field_type("date", "datetime");
		$crud->set_relation("categoryId", "category", "name");
		$crud->set_relation("userId", "user", "fullname");
		$crud->field_type("viewCount", "string");
		$crud->field_type("commentCount", "string");
		$crud->field_type("likeCount", "string");

		// Relation n-n

		// Validation
		$crud->set_rules("subTitle", "SubTitle", "required");
		$crud->set_rules("title", "Title", "required");
		$crud->set_rules("caption", "Caption", "required");
		$crud->set_rules("content", "Content", "required");
		$crud->set_rules("image", "Image", "required");
		$crud->set_rules("date", "Date", "required");
		$crud->set_rules("userId", "UserId", "required");

		// Display As
		$crud->display_as("newsId", "News Id");
		$crud->display_as("subTitle", "Sub Title");
		$crud->display_as("title", "Title");
		$crud->display_as("caption", "Caption");
		$crud->display_as("content", "Content");
		$crud->display_as("image", "File");
		$crud->display_as("date", "Date");
		$crud->display_as("categoryId", "Category");
		$crud->display_as("userId", "Author");
		$crud->display_as("viewCount", "View Count");
		$crud->display_as("commentCount", "Comment Count");
		$crud->display_as("likeCount", "Like Count");

		// Unset action

		$data = (array) $crud->render();

		$this->layout->set_wrapper( 'grocery', $data,'page', false);

		$template_data['grocery_css'] = $data['css_files'];
		$template_data['grocery_js']  = $data['js_files'];
		$template_data["title"] = "News";
		$template_data["crumb"] = ["News" => "",];
		$this->layout->auth();
		$this->layout->render('admin', $template_data); // front - auth - admin
	}
}

/* End of file example.php */
/* Location: ./application/modules/news/controllers/News.php */