<?php defined("BASEPATH") or exit("No direct script access allowed");

/**
 * Category Controller.
 */
class Category extends MY_Controller
{
    private $title;

    public function __construct()
    {
        parent::__construct();

        $this->title = "Category";
    }

    /**
     * Index
     */
    public function index()
    {
    }

    /**
     * CRUD
     */
	public function crudCategory()
	{
		$crud = new grocery_CRUD();
		
		$crud->set_table("category");
		$crud->set_subject("Category");

		// Show in
		$crud->add_fields(["name", "description", "parent"]);
		$crud->edit_fields(["name", "description", "parent"]);
		$crud->columns(["name", "description", "parent"]);

		// Fields type
		$crud->field_type("categoryId", "string");
		$crud->field_type("name", "string");
		$crud->field_type("description", "text");
		$crud->set_relation("parent", "category", "name");

		// Relation n-n

		// Validation
		$crud->set_rules("name", "Name", "required");
		$crud->set_rules("description", "Description", "required");
		$crud->set_rules("parent", "Parent", "required");

		// Display As

		// Unset action

		$data = (array) $crud->render();

		$this->layout->set_wrapper( 'grocery', $data,'page', false);

		$template_data['grocery_css'] = $data['css_files'];
		$template_data['grocery_js']  = $data['js_files'];
		$template_data["title"] = "Category";
		$template_data["crumb"] = ["Category" => "",];
		$this->layout->auth();
		$this->layout->render('admin', $template_data); // front - auth - admin
	}
}

/* End of file example.php */
/* Location: ./application/modules/category/controllers/Category.php */