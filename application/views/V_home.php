<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php 
/** format tanggal Indo**/

date_default_timezone_set('Asia/Jakarta');
function tgl_ind($date) {

/** ARRAY HARI DAN BULAN**/	
		$Hari = array("Minggu","Senin","Selasa","Rabu","Kamis","Jumat","Sabtu");
		$Bulan = array("Januari","Febuari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","Nov","Desember");
		
/** MEMISAHKAN FORMAT TANGGAL, BULAN, TAHUN, DENGAN SUBSTRING**/		
	$tahun = substr($date, 0, 4);
	$bulan = substr($date, 5, 2);
	$tgl = substr($date, 8, 2);
	$waktu = substr($date, 11, 8);		
	$hari = date("w", strtotime($date));
	
	$result = $Hari[$hari]." ".$tgl." ".$Bulan[(int)$bulan-1]." ".$tahun.", ".$waktu."";
	return $result;
	} 

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <meta property="og:site_name" content="palapapos"/>
    <meta property="og:title" content="www.palapapos.co.id" />
    <meta property="og:url" content="<?php echo current_url() ?>" />
    <meta property="og:description" content="PALAPAPOS | MENUMBUHKAN HARAPAN" />
    <meta property="og:image" content="http://www.palapapos.co.id/asset/images/home.jpeg" />
    <meta property="og:image:width" content="650" />
    <meta property="og:image:height" content="366" />
    <meta property="og:type" content="article" />
	<!--title-->
    <title>PALAPAPOS | Menumbuhkan Harapan</title>
	
	<!--CSS-->
    <link href="<?php echo base_url('asset/css/bootstrap.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('asset/css/font-awesome.min.css');?>" rel="stylesheet">
	<link href="<?php echo base_url('asset/css/magnific-popup.css');?>" rel="stylesheet">
	<link href="<?php echo base_url('asset/css/owl.carousel.css');?>" rel="stylesheet">
	<link href="<?php echo base_url('asset/css/subscribe-better.css');?>" rel="stylesheet">
	<link href="<?php echo base_url('asset/css/main2.css');?>" rel="stylesheet">
	<link id="preset" rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/presets/preset1.css');?>">
	<link href="<?php echo base_url('asset/css/responsive.css');?>" rel="stylesheet">		
	
	<!--Google Fonts-->
	<link href='https://fonts.googleapis.com/css?family=Signika+Negative:400,300,600,700' rel='stylesheet' type='text/css'>
	
    <!--[if lt IE 9]>
	    <script src="js/html5shiv.js"></script>
	    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="<?php echo base_url('asset/images/ico/favicon.png');?>">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url('asset/images/ico/apple-touch-icon-144-precomposed.png');?>">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url('asset/images/ico/apple-touch-icon-114-precomposed.png');?>">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url('asset/images/ico/apple-touch-icon-72-precomposed.png');?>">
    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url('asset/images/ico/apple-touch-icon-57-precomposed.png');?>">
</head><!--/head-->
<body>
	<div id="main-wrapper" class="homepage">
		<header id="navigation">
			<div class="navbar" role="banner">
				<div class="container">
					<a class="secondary-logo" href="<?php echo base_url();?>">
						<img class="img-responsive" src="<?php echo base_url('asset/images/presets/preset1/logo-palapapos.png');?>" alt="logo">
					</a>
				</div>
				<div class="topbar">
					<div class="container">
						<div id="topbar" class="navbar-header">							
							<a class="navbar-brand" href="<?php echo base_url('');?>">
								<img class="main-logo img-responsive" src="<?php echo base_url('asset/images/presets/preset1/logo-palapapos2.png');?>" alt="logo">
							</a>
							<div id="topbar-right">
								<!--<div id="date-time"></div>-->
							</div>
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div> 
					</div> 
				</div> 
				<div id="menubar" class="container">	
					<nav id="mainmenu" class="navbar-left collapse navbar-collapse"> 
						<ul class="nav navbar-nav"> 
							<li class="home"><a href="<?php echo base_url();?>">Home</a></li>
							<li class="home"><a href="<?php echo base_url('/Listing/kategori/0/1/politik');?>">Politik</a></li>
							<li class="home"><a href="<?php echo base_url('/Listing/kategori/0/2/nasional');?>">Nasional</a></li>
							<li class="home"><a href="<?php echo base_url('/Listing/kategori/0/4/sumut');?>">Sumut</a></li>
							<li class="home"><a href="<?php echo base_url('/Listing/kategori/0/30/tapanuli');?>">Tapanuli</a></li>
							<li class="home dropdown mega-cat-dropdown">
								<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Nusantara</a>
								<div class="dropdown-menu mega-cat-menu">
									<div class="container">
										<div class="sub-catagory">
											<h2 class="section-title">Nusantara</h2>
											<ul class="list-inline">
												<li><a href="<?php echo base_url('/Listing/kategori/3/28/sumatera');?>">Sumatera</a></li>
												<li><a href="<?php echo base_url('/Listing/kategori/3/5/jawa-barat');?>">Jawa Barat</a></li>
												<li><a href="<?php echo base_url('/Listing/kategori/3/6/jawa-tengah');?>">Jawa Tengah</a></li>
												<li><a href="<?php echo base_url('/Listing/kategori/3/7/jawa-timur');?>">Jawa Timur</a></li>
												<li><a href="<?php echo base_url('/Listing/kategori/3/8/banten');?>">Banten</a></li>
											</ul>
										</div>
										<div class="row">
								<?php
    								function time_since($time)
    								{
    									$time = time() - $time; // to get the time since that moment
    									$time = ($time<1)? 1 : $time;
    									$tokens = array (
    										31536000 => 'year',
    										2592000 => 'month',
    										604800 => 'week',
    										86400 => 'day',
    										3600 => 'hour',
    										60 => 'minute',
    										1 => 'second'
    									);
    
    									foreach ($tokens as $unit => $text) {
    										if ($time < $unit) continue;
    										$numberOfUnits = floor($time / $unit);
    										return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'');
    									}
    
    								}
									foreach ($nusantara as $i) :
										$id=$i['newsId'];
										$term=$i['categoryId'];
										$judul=$i['title'];
										$image=$i['image'];
										$isi=$i['content'];
										$tgl=$i['date'];
										$comm=$i['commentCount'];
										$view=$i['viewCount'];
										$like=$i['likeCount'];
								?>										
										
											<div class="col-sm-3">
												<div class="post medium-post">
													<div class="entry-header">
														<div class="entry-thumbnail">
															<img class="img-responsive" src="<?php echo 'http://www.palapapos.co.id/myigniter/assets/uploads/'.$image;?>" alt="" style="width:450px;height:149px;"/>
														</div>
													</div>
													<div class="post-content">								
														<div class="entry-meta">
															<ul class="list-inline">
															    <li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i><?php echo tgl_ind(date('Y-m-d H:i', strtotime($tgl))).' WIB';?></a></li>
															</ul>
														</div>
														<h2 class="entry-title">
															<a href="<?php echo base_url().'listing/view/'.$id.'/'.$term.'/'.url_title($judul, 'dash', true);?>") .'"><?php echo substr($judul,0,40);?>...</a>
														</h2>
													</div>
												</div><!--/post-->												
											</div>
								<?php endforeach;?>
										</div>
									</div>
								</div>
							</li>
							
							<li class="home dropdown mega-cat-dropdown">
								<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Megapolitan</a>
								<div class="dropdown-menu mega-cat-menu">
									<div class="container">
										<div class="sub-catagory">
											<h2 class="section-title">Megapolitan</h2>
											<ul class="list-inline">
												<li><a href="<?php echo base_url('/Listing/kategori/9/10/bekasi');?>">Bekasi</a></li>
												<li><a href="<?php echo base_url('/Listing/kategori/9/11/bogor');?>">Bogor</a></li>
												<li><a href="<?php echo base_url('/Listing/kategori/9/12/depok');?>">Depok</a></li>
												<li><a href="<?php echo base_url('/Listing/kategori/9/13/jakarta');?>">Jakarta</a></li>
												<li><a href="<?php echo base_url('/Listing/kategori/9/14/tangerang');?>">Tangerang</a></li>
											</ul>
										</div>
										<div class="row">
								<?php
									foreach ($megapolitan as $i) :
										$id=$i['newsId'];
										$term=$i['categoryId'];
										$judul=$i['title'];
										$image=$i['image'];
										$isi=$i['content'];
										$tgl=$i['date'];
										$comm=$i['commentCount'];
										$view=$i['viewCount'];
										$like=$i['likeCount'];
								?>										
										
											<div class="col-sm-3">
												<div class="post medium-post">
													<div class="entry-header">
														<div class="entry-thumbnail">
															<img class="img-responsive" src="<?php echo 'http://www.palapapos.co.id/myigniter/assets/uploads/'.$image;?>" alt="" style="width:450px;height:149px;"/>
														</div>
													</div>
													<div class="post-content">								
														<div class="entry-meta">
															<ul class="list-inline">
																<li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i><?php echo tgl_ind(date('Y-m-d H:i', strtotime($tgl))).' WIB';?></a></li>
															</ul>
														</div>
														<h2 class="entry-title">
															<a href="<?php echo base_url().'listing/view/'.$id.'/'.$term.'/'.url_title($judul, 'dash', true);?>") .'"><?php echo substr($judul,0,40);?>...</a>
														</h2>
													</div>
												</div><!--/post-->												
											</div>
								<?php endforeach;?>
										</div>
									</div>
								</div>
							</li>
							<li class="home dropdown mega-cat-dropdown">
								<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Hukum</a>
								<div class="dropdown-menu mega-cat-menu">
									<div class="container">
										<div class="sub-catagory">
											<h2 class="section-title">Hukum</h2>
											<ul class="list-inline">
												<li><a href="<?php echo base_url('/Listing/kategori/15/16/kriminal');?>">Kriminal</a></li>
												<li><a href="<?php echo base_url('/Listing/kategori/15/17/peristiwa');?>">Peristiwa</a></li>
												<li><a href="<?php echo base_url('/Listing/kategori/15/18/korupsi');?>">Korupsi</a></li>
											</ul>
										</div>
										<div class="row">
								<?php
									foreach ($hukum as $i) :
										$id=$i['newsId'];
										$term=$i['categoryId'];
										$judul=$i['title'];
										$image=$i['image'];
										$isi=$i['content'];
										$tgl=$i['date'];
										$comm=$i['commentCount'];
										$view=$i['viewCount'];
										$like=$i['likeCount'];
								?>										
										
											<div class="col-sm-3">
												<div class="post medium-post">
													<div class="entry-header">
														<div class="entry-thumbnail">
															<img class="img-responsive" src="<?php echo 'http://www.palapapos.co.id/myigniter/assets/uploads/'.$image;?>" alt="" style="width:450px;height:149px;" />
														</div>
													</div>
													<div class="post-content">								
														<div class="entry-meta">
															<ul class="list-inline">
																<li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i><?php echo tgl_ind(date('Y-m-d H:i', strtotime($tgl))).' WIB';?></a></li>
															</ul>
														</div>
														<h2 class="entry-title">
															<a href="<?php echo base_url().'listing/view/'.$id.'/'.$term.'/'.url_title($judul, 'dash', true);?>") .'"><?php echo substr($judul,0,40);?>...</a>
														</h2>
													</div>
												</div><!--/post-->												
											</div>
								<?php endforeach;?>
										</div>
									</div>
								</div>
							</li>
							<li class="home dropdown mega-cat-dropdown">
								<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Ekonomi</a>
								<div class="dropdown-menu mega-cat-menu">
									<div class="container">
										<div class="sub-catagory">
											<h2 class="section-title">Ekonomi</h2>
											<ul class="list-inline">
												<li><a href="<?php echo base_url('/Listing/kategori/19/20/makro');?>">Makro</a></li>
												<li><a href="<?php echo base_url('/Listing/kategori/19/21/mikro');?>">Mikro</a></li>
											</ul>
										</div>
										<div class="row">
								<?php
									foreach ($ekonomi as $i) :
										$id=$i['newsId'];
										$term=$i['categoryId'];
										$judul=$i['title'];
										$image=$i['image'];
										$isi=$i['content'];
										$tgl=$i['date'];
										$comm=$i['commentCount'];
										$view=$i['viewCount'];
										$like=$i['likeCount'];
								?>										
										
											<div class="col-sm-3">
												<div class="post medium-post">
													<div class="entry-header">
														<div class="entry-thumbnail">
															<img class="img-responsive" src="<?php echo 'http://www.palapapos.co.id/myigniter/assets/uploads/'.$image;?>" alt="" style="width:450px;height:149px;" />
														</div>
													</div>
													<div class="post-content">								
														<div class="entry-meta">
															<ul class="list-inline">
																<li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i><?php echo tgl_ind(date('Y-m-d H:i', strtotime($tgl))).' WIB';?></a></li>
															</ul>
														</div>
														<h2 class="entry-title">
															<a href="<?php echo base_url().'listing/view/'.$id.'/'.$term.'/'.url_title($judul, 'dash', true);?>") .'"><?php echo substr($judul,0,40);?>...</a>
														</h2>
													</div>
												</div><!--/post-->												
											</div>
								<?php endforeach;?>
										</div>
									</div>
								</div>
							</li>
							<li class="home dropdown mega-cat-dropdown">
								<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Hiburan</a>
								<div class="dropdown-menu mega-cat-menu">
									<div class="container">
										<div class="sub-catagory">
											<h2 class="section-title">Hiburan</h2>
											<ul class="list-inline">
												<li><a href="<?php echo base_url('/Listing/kategori/22/23/kuliner');?>">Kuliner</a></li>
												<li><a href="<?php echo base_url('/Listing/kategori/22/24/wisata');?>">Wisata</a></li>
												<li><a href="<?php echo base_url('/Listing/kategori/22/25/event');?>">Event</a></li>
												<li><a href="<?php echo base_url('/Listing/kategori/22/27/budaya');?>">Budaya</a></li>
											</ul>
										</div>
										<div class="row">
								<?php
									foreach ($hiburan as $i) :
										$id=$i['newsId'];
										$term=$i['categoryId'];
										$judul=$i['title'];
										$image=$i['image'];
										$isi=$i['content'];
										$tgl=$i['date'];
										$comm=$i['commentCount'];
										$view=$i['viewCount'];
										$like=$i['likeCount'];
								?>										
										
											<div class="col-sm-3">
												<div class="post medium-post">
													<div class="entry-header">
														<div class="entry-thumbnail">
															<img class="img-responsive" src="<?php echo 'http://www.palapapos.co.id/myigniter/assets/uploads/'.$image;?>" alt="" style="width:450px;height:149px;" />
														</div>
													</div>
													<div class="post-content">								
														<div class="entry-meta">
															<ul class="list-inline">
																<li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i><?php echo tgl_ind(date('Y-m-d H:i', strtotime($tgl))).' WIB';?></a></li>
															</ul>
														</div>
														<h2 class="entry-title">
															<a href="<?php echo base_url().'listing/view/'.$id.'/'.$term.'/'.url_title($judul, 'dash', true);?>") .'"><?php echo substr($judul,0,40);?>...</a>
														</h2>
													</div>
												</div><!--/post-->												
											</div>
								<?php endforeach;?>
										</div>
									</div>
								</div>
							</li>
							<li class="home"><a href="<?php echo base_url('/Listing/kategori/0/26/olahraga');?>">Olahraga</a></li>
							<li class="home"><a href="<?php echo base_url('/Listing/kategori/0/29/advertorial');?>">Advertorial</a></li>
						</ul> 						
					</nav>
					<div class="searchNlogin">
						<ul>
							<li class="search-icon"><i class="fa fa-search"></i></li>
							<li class="dropdown user-panel"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i></a>
								<div class="dropdown-menu top-user-section">
									<div class="top-user-form">
										<form id="top-login" role="form">
											<div class="input-group" id="top-login-username">
												<span class="input-group-addon"><img src="<?php echo base_url('asset/images/others/user-icon.png');?>" alt="" /></span>
												<input type="text" class="form-control" placeholder="Username" required="">
											</div>
											<div class="input-group" id="top-login-password">
												<span class="input-group-addon"><img src="<?php echo base_url('asset/images/others/password-icon.png');?>" alt="" /></span>
												<input type="password" class="form-control" placeholder="Password" required="">
											</div>
											<div>
												<p class="reset-user">Forgot <a href="#">Password/Username?</a></p>
												<button class="btn btn-danger" type="submit">Login</button>
											</div>
										</form>
									</div>
									<div class="create-account">
										<a href="#">Create a New Account</a>
									</div>
								</div>
							</li>
						</ul>
						<div class="search">
							<form role="form">
								<input type="text" class="search-form" autocomplete="off" placeholder="Type & Press Enter">
							</form>
						</div> <!--/.search--> 
					</div><!-- searchNlogin -->
				</div>
			</div>
		</header>
		
    
        <?php $this->load->view('V_homeslider');?>
		
		<div class="container">
			<div class="section add inner-add">
				<a href="#"><img class="img-responsive" src="<?php echo base_url('asset/images/iklan/Advertise2.png');?>" alt="" /></a>
			</div><!--/.section-->
			
            <?php $this->load->view('V_hometerpopuler');?>
			
			<div class="section add inner-add">
				<a href="#"><img class="img-responsive" src="<?php echo base_url('asset/images/iklan/Advertise2.png');?>" alt="" /></a>
			</div><!--/.section-->

			<div class="section">
				<div class="row">
					<div class="col-md-9 col-sm-8">
						<div id="site-content">
							<div class="row">
								<div class="col-md-8 col-sm-6">
									<div class="left-content">
										<div class="section health-section">
											<h1 class="section-title">Sumut</h1>
											<div class="cat-menu">         
												<ul class="list-inline">                       
												<li><a href="<?php echo base_url('/Listing/kategori/0/4/sumut');?>">See All</a></li>
												</ul> 					
											</div>
											
										<?php
											foreach ($sumutslide4 as $i) :
												$id=$i['newsId'];
												$term=$i['categoryId'];
												$judul=$i['title'];
												$image=$i['image'];
												$isi=$i['content'];
												$tgl=$i['date'];
												$comm=$i['commentCount'];
												$view=$i['viewCount'];
												$like=$i['likeCount'];
										?>																						
											<div class="health-feature">
												<div class="post">
													<div class="entry-header">
														<div class="entry-thumbnail">														
															<img class="img-responsive" src="<?php echo 'http://www.palapapos.co.id/myigniter/assets/uploads/'.$image;?>" alt="" height="258" width="450" />
														</div>
													</div>
													<div class="post-content">								
														<div class="entry-meta">
															<ul class="list-inline">
																<li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i><?php echo tgl_ind(date('Y-m-d H:i', strtotime($tgl))).' WIB';?></a></li>
															</ul>
														</div>
														<h2 class="entry-title">
															<a href="<?php echo base_url().'listing/view/'.$id.'/'.$term.'/'.url_title($judul, 'dash', true);?>"><?php echo substr($judul,0,65);?></a>
														</h2>
													</div>
												</div><!--/post--> 
											</div>
										<?php endforeach;?>
											
										</div><!--/.health-section -->
										<div class="section health-section">
											<h1 class="section-title">Nasional</h1>
											<div class="cat-menu">         
												<ul class="list-inline">                       
												<li><a href="<?php echo base_url('/Listing/kategori/0/4/sumut');?>">See All</a></li>
												</ul> 					
											</div>
											
										<?php
											foreach ($nasionalslide4 as $i) :
												$id=$i['newsId'];
												$term=$i['categoryId'];
												$judul=$i['title'];
												$image=$i['image'];
												$isi=$i['content'];
												$tgl=$i['date'];
												$comm=$i['commentCount'];
												$view=$i['viewCount'];
												$like=$i['likeCount'];
										?>																						
											<div class="health-feature">
												<div class="post">
													<div class="entry-header">
														<div class="entry-thumbnail">														
															<img class="img-responsive" src="<?php echo 'http://www.palapapos.co.id/myigniter/assets/uploads/'.$image;?>" alt="" height="258" width="450" />
														</div>
													</div>
													<div class="post-content">								
														<div class="entry-meta">
															<ul class="list-inline">
																<li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i><?php echo tgl_ind(date('Y-m-d H:i', strtotime($tgl))).' WIB';?></a></li>
															</ul>
														</div>
														<h2 class="entry-title">
															<a href="<?php echo base_url().'listing/view/'.$id.'/'.$term.'/'.url_title($judul, 'dash', true);?>"><?php echo substr($judul,0,65);?></a>
														</h2>
													</div>
												</div><!--/post--> 
											</div>
										<?php endforeach;?>
											
										</div><!--/.health-section -->										
										<div class="section health-section">
											<h1 class="section-title">Politik</h1>
											<div class="cat-menu">         
												<a href="<?php echo base_url('/Listing/kategori/0/1');?>">See all</a>					
											</div>
											
										<?php
											foreach ($politik2 as $i) :
												$id=$i['newsId'];
												$term=$i['categoryId'];
												$judul=$i['title'];
												$image=$i['image'];
												$isi=$i['content'];
												$tgl=$i['date'];
												$comm=$i['commentCount'];
												$view=$i['viewCount'];
												$like=$i['likeCount'];
										?>																						
											<div class="health-feature">
												<div class="post">
													<div class="entry-header">
														<div class="entry-thumbnail">														
															<img class="img-responsive" src="<?php echo 'http://www.palapapos.co.id/myigniter/assets/uploads/'.$image;?>" alt="" height="258" width="450" />
														</div>
													</div>
													<div class="post-content">								
														<div class="entry-meta">
															<ul class="list-inline">
																<li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i><?php echo tgl_ind(date('Y-m-d H:i', strtotime($tgl))).' WIB';?></a></li>
															</ul>
														</div>
														<h2 class="entry-title">
															<a href="<?php echo base_url().'listing/view/'.$id.'/'.$term.'/'.url_title($judul, 'dash', true);?>"><?php echo substr($judul,0,65);?></a>
														</h2>
													</div>
												</div><!--/post--> 
											</div>
										<?php endforeach;?>
											
										</div><!--/.health-section -->	
										
										<div class="section health-section">
											<h1 class="section-title">Nusantara</h1>
											<div class="cat-menu">         
												<ul class="list-inline">                       
												<li><a href="<?php echo base_url('/Listing/kategori/3/4');?>">Sumatra Utara</a></li>
												<li><a href="<?php echo base_url('/Listing/kategori/3/5');?>">Jawa Barat</a></li>
												<li><a href="<?php echo base_url('/Listing/kategori/3/6');?>">Jawa Tengah</a></li>
												<li><a href="<?php echo base_url('/Listing/kategori/3/7');?>">Jawa Timur</a></li>
												<li><a href="<?php echo base_url('/Listing/kategori/3/8');?>">Banten</a></li>
												</ul> 					
											</div>
											
										<?php
											foreach ($nusantara2 as $i) :
												$id=$i['newsId'];
												$term=$i['categoryId'];
												$judul=$i['title'];
												$image=$i['image'];
												$isi=$i['content'];
												$tgl=$i['date'];
												$comm=$i['commentCount'];
												$view=$i['viewCount'];
												$like=$i['likeCount'];
										?>																						
											<div class="health-feature">
												<div class="post">
													<div class="entry-header">
														<div class="entry-thumbnail">														
															<img class="img-responsive" src="<?php echo 'http://www.palapapos.co.id/myigniter/assets/uploads/'.$image;?>" alt="" height="258" width="450" />
														</div>
													</div>
													<div class="post-content">								
														<div class="entry-meta">
															<ul class="list-inline">
																<li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i><?php echo tgl_ind(date('Y-m-d H:i', strtotime($tgl))).' WIB';?></a></li>
															</ul>
														</div>
														<h2 class="entry-title">
															<a href="<?php echo base_url().'listing/view/'.$id.'/'.$term.'/'.url_title($judul, 'dash', true);?>"><?php echo substr($judul,0,65);?></a>
														</h2>
													</div>
												</div><!--/post--> 
											</div>
										<?php endforeach;?>
											
										</div><!--/.health-section -->										
										
	
										

										
										<div class="section health-section">
											<h1 class="section-title">Megapolitan</h1>
											<div class="cat-menu">         
												<ul class="list-inline">                       
												<li><a href="<?php echo base_url('/Listing/kategori/9/10');?>">Bekasi</a></li>
												<li><a href="<?php echo base_url('/Listing/kategori/9/11');?>">Bogor</a></li>
												<li><a href="<?php echo base_url('/Listing/kategori/9/12');?>">Depok</a></li>
												<li><a href="<?php echo base_url('/Listing/kategori/9/13');?>">Jakarta</a></li>
												<li><a href="<?php echo base_url('/Listing/kategori/9/14');?>">Tangerang</a></li>
												</ul> 					
											</div>
											
										<?php
											foreach ($megapolitan2 as $i) :
												$id=$i['newsId'];
												$term=$i['categoryId'];
												$judul=$i['title'];
												$image=$i['image'];
												$isi=$i['content'];
												$tgl=$i['date'];
												$comm=$i['commentCount'];
												$view=$i['viewCount'];
												$like=$i['likeCount'];
										?>																						
											<div class="health-feature">
												<div class="post">
													<div class="entry-header">
														<div class="entry-thumbnail">														
															<img class="img-responsive" src="<?php echo 'http://www.palapapos.co.id/myigniter/assets/uploads/'.$image;?>" alt="" height="258" width="450" />
														</div>
													</div>
													<div class="post-content">								
														<div class="entry-meta">
															<ul class="list-inline">
																<li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i><?php echo tgl_ind(date('Y-m-d H:i', strtotime($tgl))).' WIB';?></a></li>
															</ul>
														</div>
														<h2 class="entry-title">
															<a href="<?php echo base_url().'listing/view/'.$id.'/'.$term.'/'.url_title($judul, 'dash', true);?>"><?php echo substr($judul,0,65);?></a>
														</h2>
													</div>
												</div><!--/post--> 
											</div>
										<?php endforeach;?>
											
										</div><!--/.health-section -->


                                        <div class="section add inner-add">
											<a href="#"><img class="img-responsive" src="<?php echo base_url('asset/images/iklan/Advertise4.png');?>" alt="" /></a>
										</div><!--/.section add-->
										<div class="section health-section">
											<h1 class="section-title">Hukum</h1>
											<div class="cat-menu">         
												<ul class="list-inline">                       
												<li><a href="<?php echo base_url('/Listing/kategori/15/16');?>">Kriminal</a></li>
												<li><a href="<?php echo base_url('/Listing/kategori/15/17');?>">Peristiwa</a></li>
												<li><a href="<?php echo base_url('/Listing/kategori/15/18');?>">Korupsi</a></li>
												</ul> 					
											</div>
											
										<?php
											foreach ($hukum2 as $i) :
												$id=$i['newsId'];
												$term=$i['categoryId'];
												$judul=$i['title'];
												$image=$i['image'];
												$isi=$i['content'];
												$tgl=$i['date'];
												$comm=$i['commentCount'];
												$view=$i['viewCount'];
												$like=$i['likeCount'];
										?>																						
											<div class="health-feature">
												<div class="post">
													<div class="entry-header">
														<div class="entry-thumbnail">														
															<img class="img-responsive" src="<?php echo 'http://www.palapapos.co.id/myigniter/assets/uploads/'.$image;?>" alt="" height="258" width="450" />
														</div>
													</div>
													<div class="post-content">								
														<div class="entry-meta">
															<ul class="list-inline">
																<li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i><?php echo tgl_ind(date('Y-m-d H:i', strtotime($tgl))).' WIB';?></a></li>
															</ul>
														</div>
														<h2 class="entry-title">
															<a href="<?php echo base_url().'listing/view/'.$id.'/'.$term.'/'.url_title($judul, 'dash', true);?>"><?php echo substr($judul,0,65);?></a>
														</h2>
													</div>
												</div><!--/post--> 
											</div>
										<?php endforeach;?>
											
										</div><!--/.health-section -->

										<div class="section health-section">
											<h1 class="section-title">Ekonomi</h1>
											<div class="cat-menu">         
												<ul class="list-inline">                       
												<li><a href="<?php echo base_url('/Listing/kategori/19/20');?>">Makro</a></li>
												<li><a href="<?php echo base_url('/Listing/kategori/19/21');?>">Mikro</a></li>
												</ul> 					
											</div>
											
										<?php
											foreach ($ekonomi2 as $i) :
												$id=$i['newsId'];
												$term=$i['categoryId'];
												$judul=$i['title'];
												$image=$i['image'];
												$isi=$i['content'];
												$tgl=$i['date'];
												$comm=$i['commentCount'];
												$view=$i['viewCount'];
												$like=$i['likeCount'];
										?>																						
											<div class="health-feature">
												<div class="post">
													<div class="entry-header">
														<div class="entry-thumbnail">														
															<img class="img-responsive" src="<?php echo 'http://www.palapapos.co.id/myigniter/assets/uploads/'.$image;?>" alt="" height="258" width="450" />
														</div>
													</div>
													<div class="post-content">								
														<div class="entry-meta">
															<ul class="list-inline">
																<li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i><?php echo tgl_ind(date('Y-m-d H:i', strtotime($tgl))).' WIB';?></a></li>
															</ul>
														</div>
														<h2 class="entry-title">
															<a href="<?php echo base_url().'listing/view/'.$id.'/'.$term.'/'.url_title($judul, 'dash', true);?>"><?php echo substr($judul,0,65);?></a>
														</h2>
													</div>
												</div><!--/post--> 
											</div>
										<?php endforeach;?>
											
										</div><!--/.health-section -->


										
										
										<div class="section add inner-add">
											<a href="#"><img class="img-responsive" src="<?php echo base_url('asset/images/iklan/Advertise4.png');?>" alt="" /></a>
										</div><!--/.section add-->
										
									</div><!--/.left-content-->
								</div>
								<div class="col-md-4 col-sm-6">
									<div class="middle-content">
										<div class="section sports-section">
											<h1 class="section-title title">Tapanuli</h1>
											<div class="cat-menu">         
												<a href="<?php echo base_url('/Listing/kategori/0/2');?>">See all</a>					
											</div>	

								<?php
									foreach ($tapanuli4 as $i) :
										$id=$i['newsId'];
										$term=$i['categoryId'];
										$judul=$i['title'];
										$image=$i['image'];
										$isi=$i['content'];
										$tgl=$i['date'];
										$comm=$i['commentCount'];
										$view=$i['viewCount'];
										$like=$i['likeCount'];
								?>
								
											<div class="post medium-post">
												<div class="entry-header">
													<div class="entry-thumbnail">
														<img class="img-responsive" src="<?php echo 'http://www.palapapos.co.id/myigniter/assets/uploads/'.$image;?>" alt="" height="158" width="273" />
													</div>
												</div>
												<div class="post-content">								
													<div class="entry-meta">
														<ul class="list-inline">
															<li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i><?php echo tgl_ind(date('Y-m-d H:i', strtotime($tgl))).' WIB';?></a></li>
														</ul>
													</div>
													<h2 class="entry-title">
														<a href="<?php echo base_url().'listing/view/'.$id.'/'.$term.'/'.url_title($judul, 'dash', true);?>") .'"><?php echo substr($judul,0,60);?></a>
													</h2>
												</div>
											</div><!--/post--> 

								<?php endforeach;?>			
										</div><!--/.sports-section -->									

										
										<div class="section">
											<div class="add inner-add">
												<a href="#"><img class="img-responsive" src="<?php echo base_url('asset/images/iklan/iklansukurhnababan2.jpg');?>" alt="" /></a>
											</div>
										</div>
										

										
										<div class="section business-section">
											<h1 class="section-title">Olahraga</h1>
											<div class="cat-menu">         
												<a href="<?php echo base_url('/Listing/kategori/0/26');?>">See all</a>					
											</div>

                								<?php
                									foreach ($olahragaslide4 as $i) :
                										$id=$i['newsId'];
                										$term=$i['categoryId'];
                										$judul=$i['title'];
                										$image=$i['image'];
                										$isi=$i['content'];
                										$tgl=$i['date'];
                										$comm=$i['commentCount'];
                										$view=$i['viewCount'];
                										$like=$i['likeCount'];
                								?>
								
											<div class="post medium-post">
												<div class="entry-header">
													<div class="entry-thumbnail">
														<img class="img-responsive" src="<?php echo 'http://www.palapapos.co.id/myigniter/assets/uploads/'.$image;?>" alt="" height="158" width="273" />
													</div>
												</div>
												<div class="post-content">								
													<div class="entry-meta">
														<ul class="list-inline">
															<li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i><?php echo tgl_ind(date('Y-m-d H:i', strtotime($tgl))).' WIB';?></a></li>
														</ul>
													</div>
													<h2 class="entry-title">
														<a href="<?php echo base_url().'listing/view/'.$id.'/'.$term.'/'.url_title($judul, 'dash', true);?>") .'"><?php echo substr($judul,0,60);?></a>
													</h2>
												</div>
											</div><!--/post--> 

								            <?php endforeach;?>													

										</div><!-- /.business-section -->
										<div class="section">
											<div class="add inner-add">
												<a href="#"><img class="img-responsive" src="<?php echo base_url('asset/images/iklan/Advertise3.png');?>" alt="" /></a>
											</div>
										</div>
										<div class="section video-section">
											<h1 class="section-title title">Video</h1>
											<div class="cat-menu">         
												<a href="https://www.youtube.com/channel/UCBCLbed21gfakiR8gqxMtow">See all</a>					
											</div>
											<div class="post video-post medium-post">
												<div class="entry-header">
													<div class="entry-thumbnail embed-responsive embed-responsive-16by9">
														<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/9-PVAc68CUU" allowfullscreen></iframe>
													</div>
												</div>
												<div class="post-content">								
													<div class="video-catagory"><a href="#">Hiburan</a></div>
													<h2 class="entry-title">
														<a href="news-details.html">Pawai Pembukaan Festival Danau Toba 2016 di Kecamatan Muara Kabupaten Tapanuli Utara Provinsi Sumatera Utara.</a>
													</h2>
												</div>
											</div><!--/post-->
											<div class="post video-post medium-post">
												<div class="entry-header">
													<div class="entry-thumbnail embed-responsive embed-responsive-16by9">
														<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/5p27p9Jqa3g" allowfullscreen></iframe>
													</div>
												</div>
												<div class="post-content">								
													<div class="video-catagory"><a href="#">Hiburan</a></div>
													<h2 class="entry-title">
														<a href="news-details.html">Tari Empat Puak yang ditampilkan oleh siswa SMA N 1 Muara Tapanuli Utara Sumatera Utara memukau pengunjung pada pembukaan Festival Danau Toba 2016. </a>
													</h2>
												</div>
											</div><!--/post-->			
																			
										</div> <!-- /.video-section -->										
										<div class="section business-section">
											<h1 class="section-title">Hiburan</h1>
											<div class="cat-menu">         
												<ul class="list-inline">
													<li><a href="<?php echo base_url('/Listing/kategori/22/23');?>">Kuliner</a></li>
													<li><a href="<?php echo base_url('/Listing/kategori/22/24');?>">Wisata</a></li>
													<li><a href="<?php echo base_url('/Listing/kategori/22/25');?>">Event</a></li>
													<li><a href="<?php echo base_url('/Listing/kategori/22/27');?>">Budaya</a></li>
												</ul>					
											</div>

                								<?php
                									foreach ($hiburan2 as $i) :
                										$id=$i['newsId'];
                										$term=$i['categoryId'];
                										$judul=$i['title'];
                										$image=$i['image'];
                										$isi=$i['content'];
                										$tgl=$i['date'];
                										$comm=$i['commentCount'];
                										$view=$i['viewCount'];
                										$like=$i['likeCount'];
                								?>
								
											<div class="post medium-post">
												<div class="entry-header">
													<div class="entry-thumbnail">
														<img class="img-responsive" src="<?php echo 'http://www.palapapos.co.id/myigniter/assets/uploads/'.$image;?>" alt="" height="158" width="273" />
													</div>
												</div>
												<div class="post-content">								
													<div class="entry-meta">
														<ul class="list-inline">
															<li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i><?php echo tgl_ind(date('Y-m-d H:i', strtotime($tgl))).' WIB';?></a></li>
														</ul>
													</div>
													<h2 class="entry-title">
														<a href="<?php echo base_url().'listing/view/'.$id.'/'.$term.'/'.url_title($judul, 'dash', true);?>") .'"><?php echo substr($judul,0,60);?></a>
													</h2>
												</div>
											</div><!--/post--> 

								<?php endforeach;?>													

										</div><!-- /.business-section -->
										
										
									</div><!--/.middle-content-->
								</div>
							</div>
						</div><!--/#site-content-->
					</div>
					<div class="col-md-3 col-sm-4">
						<div id="sitebar">
							<div class="widget follow-us">
								<h1 class="section-title title">Media Sosial</h1>
								<ul class="list-inline social-icons">
									<li><a href="#"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
									<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
									<li><a href="#"><i class="fa fa-youtube"></i></a></li>
								</ul>
							</div><!--/#widget-->
							
							<div class="widget">
								<div class="add">
									<a href="#"><img class="img-responsive" src="<?php echo base_url('asset/images/iklan/iklansukurhnababan.jpg');?>" alt="" /></a>
								</div>
							</div><!--/#widget-->
							
							<div class="widget">
								<h1 class="section-title title">Indeks</h1>
								<ul class="post-list">
								
								
								<?php
									foreach ($posts4 as $i) :
										$term=$i['name'];
										$judul=$i['title'];
										$image=$i['image'];
										$id=$i['newsId'];
										$term_id=$i['categoryId'];
								?>
									<li>
										<div class="post small-post">
											<div class="entry-header">
												<div class="entry-thumbnail">
													<img class="img-responsive" src="<?php echo 'http://www.palapapos.co.id/myigniter/assets/uploads/'.$image;?>" alt="" height="95" width="95" />
												</div>
											</div>
											<div class="post-content">					
												<h2 class="entry-title">
													<a href="<?php echo base_url().'listing/view/'.$id.'/'.$term_id.'/'.url_title($judul, 'dash', true);?>") .'"><?php echo $judul;?></a>
												</h2>
											</div>
										</div><!--/post-->
									</li>
								<?php endforeach;?>
									
									
								</ul>
							</div><!--/#widget-->
							
							<div class="widget">
								<div class="add">
									<a href="#"><img class="img-responsive" src="<?php echo base_url('asset/images/iklan/Advertise3.png');?>" alt="" /></a>
								</div>
							</div><!--/#widget-->
													
							<div class="widget meta-widget">
								<h1 class="section-title title">Tambahan</h1>
								<div class="meta-tab">
									<ul class="nav nav-tabs nav-justified" role="tablist">
										<li role="presentation"><a href="#author" data-toggle="tab"><i class="fa fa-user"></i>Authors</a></li>
										<li role="presentation"><a href="#comment" data-toggle="tab"><i class="fa fa-comment-o"></i>Comments</a></li>
										<li role="presentation" class="active"><a href="#tag" data-toggle="tab"><i class="fa fa-inbox"></i>Tag</a></li>
									</ul>
									<div class="tab-content">
										<div role="tabpanel" class="tab-pane" id="author">
											<ul class="authors-post">	
											<?php
												foreach ($widget1 as $i) :
													$id=$i['newsId'];
													$term=$i['categoryId'];
													$judul=$i['title'];
													$image=$i['image'];
													$isi=$i['content'];
													$tgl=$i['date'];
													$author=$i['fullname'];
											?>											
												<li>
													<div class="post small-post">
														<div class="post-content">	
															<div class="entry-meta">
																<ul class="list-inline">
																	<li class="post-author"><a href="#"><?php echo $author;?>,</a></li>
																	<li class="publish-date"><a href="#"><?php echo time_since(strtotime($tgl)).' ago';?> </a></li>
																</ul>
															</div>
															<h2 class="entry-title">
																<a href="<?php echo base_url().'listing/view/'.$id.'/'.$term.'/'.url_title($judul, 'dash', true);?>") .'"><?php echo substr($judul,0,50);?></a>
															</h2>
														</div>
													</div><!--/post-->
												</li>
											<?php endforeach;?>
											</ul>
										</div>
										<div role="tabpanel" class="tab-pane" id="comment">
											<ul class="comment-list">
											<?php
												foreach ($widget2 as $i) :
													$author=$i['author'];
													$tgl=$i['date'];
													$isi=$i['content'];
													
											?>												
												<li>
													<div class="post small-post">
														<div class="post-content">	
															<div class="entry-meta">
																<ul class="list-inline">
																	<li class="post-author"><a href="#"><?php echo $author;?> ,</a></li>
																	<li class="publish-date"><a href="#"><?php echo time_since(strtotime($tgl)).' ago';?> </a></li>
																</ul>
															</div>
															<h2 class="entry-title">
																<a href="#"><?php echo substr($isi,0,50);?></a>
															</h2>
														</div>
													</div><!--/post-->
												</li>
											<?php endforeach;?>

											</ul>
										</div>
										<div role="tabpanel" class="tab-pane active" id="tag">
											<ul class="list-inline tag-cloud">
												<?php
													foreach ($widget3 as $i) :
														$tag=$i['name'];
												?>
													<li><a href="#"><?php echo $tag;?></a>,</li>
												<?php endforeach;?>
											</ul>
										</div>
									</div>
								</div>
							</div><!--/#widget-->
							
						</div><!--/#sitebar-->
					</div>
				</div>				
			</div><!--/.section-->
			<div class="section add inner-add">
				<a href="#"><img class="img-responsive" src="<?php echo base_url('asset/images/iklan/Advertise2.png');?>" alt="" /></a>
			</div><!--/.section-->
		</div><!--/.container-->
	</div><!--/#main-wrapper--> 
	
	<footer id="footer">
		<div class="footer-top">
			<div class="container text-center">
				<div class="logo-icon"><img class="img-responsive" src="<?php echo base_url('asset/images/presets/preset1/logo-icon-palapapos.png');?>" alt="" /></div>
			</div>
		</div>
		<div class="footer-menu">
			<div class="container">
				<ul class="nav navbar-nav">                       
					<li><a href="<?php echo base_url('/Listing/content/2');?>">Tentang Kita</a></li>
					<li><a href="<?php echo base_url('/Listing/content/3');?>">Redaksi</a></li>
					<li><a href="<?php echo base_url('/Listing/content/4');?>">Pedoman Media Siber</a></li>
					<li><a href="<?php echo base_url('/Listing/content/5');?>">Karir</a></li>
					<li><a href="<?php echo base_url('/Listing/content/6');?>">Hubungi Kami</a></li>
					<li><a href="<?php echo base_url('/Listing/content/7');?>">Info Iklan</a></li>
					<li><a href="<?php echo base_url('/Listing/content/8');?>">Privacy Policy</a></li>
					<li><a href="<?php echo base_url('/Listing/content/9');?>">Disclaimer</a></li>
				</ul> 
			</div>
		</div>

		<div class="footer-bottom">
			<div class="container text-center">
				<p><a href="#">PALAPAPOS </a>&copy; 2018 </p>
			</div>
		</div>		
	</footer>
	
	
  <!--/subscribe-me--> 
	
	<!--/Preset Style Chooser
	<div class="style-chooser">
		<div class="style-chooser-inner">
			<a href="#" class="toggler"><i class="fa fa-life-ring fa-spin"></i></a>
			<h4>Presets Color</h4>
			<ul class="preset-list clearfix">
				<li class="preset1 active" data-preset="1"><a href="#" data-color="preset1"></a></li>
				<li class="preset2" data-preset="2"><a href="#" data-color="preset2"></a></li>
				<li class="preset3" data-preset="3"><a href="#" data-color="preset3"></a></li>        
				<li class="preset4" data-preset="4"><a href="#" data-color="preset4"></a></li>
				<li class="preset5" data-preset="5"><a href="#" data-color="preset5"></a></li>
				<li class="preset6" data-preset="6"><a href="#" data-color="preset6"></a></li>
			</ul>
		</div>
    </div>
	/End:Preset Style Chooser-->
		
	<!--/#scripts--> 
    <script type="text/javascript" src="<?php echo base_url('asset/js/jquery.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/js/bootstrap.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('asset/js/jquery.magnific-popup.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('asset/js/owl.carousel.min.js');?>"></script> 
	<script type="text/javascript" src="<?php echo base_url('asset/js/moment.min.js');?>"></script> 
	<script type="text/javascript" src="<?php echo base_url('asset/js/jquery.simpleWeather.min.js');?>"></script> 
	<script type="text/javascript" src="<?php echo base_url('asset/js/jquery.sticky-kit.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('asset/js/jquery.easy-ticker.min.js');?>"></script> 
	<script type="text/javascript" src="<?php echo base_url('asset/js/jquery.subscribe-better.min.js');?>"></script> 
    <script type="text/javascript" src="<?php echo base_url('asset/js/main.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/js/switcher.js');?>"></script>

</body>
</html>