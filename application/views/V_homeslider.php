		<h1 class="section-title">Berita Terkini</h1>
		<div id="main-slider">
			<?php
				foreach ($beritaterkini as $i) :
					$id=$i['newsId'];
					$term=$i['categoryId'];
					$kat=$i['name'];
					$judul=$i['title'];
					$image=$i['image'];
					$isi=$i['content'];
					$tgl=$i['date'];
					$comm=$i['commentCount'];
					$view=$i['viewCount'];
					$like=$i['likeCount'];
			?>	
        			<div class="post feature-post" style="background-image:url(<?php echo 'http://www.palapapos.co.id/myigniter/assets/uploads/'.$image;?>); background-size:cover;">				
        				<div class="post-content">
            				<div class="entry-meta">
            					<ul class="list-inline">
            						<li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i><?php echo tgl_ind(date('Y-m-d H:i', strtotime($tgl))).' WIB';?></a></li>
            					</ul>
            				</div>
        					<h2 class="entry-title">
        						<a href="<?php echo base_url().'index.php/listing/view/'.$id.'/'.$term.'/'.url_title($judul, 'dash', true);?>"><?php echo $judul;?></a>
        					</h2>
        				</div>
        			</div>
			<?php endforeach;?>
		</div>