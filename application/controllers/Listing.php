<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Listing extends CI_Controller {

	public function __construct(){
        parent::__construct();
       $this->load->model('Common_model');
    }

    public function index()
    {
        $data = array();
		$data['nusantara'] = $this->Common_model->get_limit_nusantara();
		$data['megapolitan'] = $this->Common_model->get_limit_megapolitan();
		$data['hukum'] = $this->Common_model->get_limit_hukum();
		$data['ekonomi'] = $this->Common_model->get_limit_ekonomi();
		$data['hiburan'] = $this->Common_model->get_limit_hiburan();
    }

	public function kategori()
	{
		$kode=$this->uri->segment(4);

		$data = array();
		$data['nusantara'] = $this->Common_model->get_limit_nusantara();
		$data['megapolitan'] = $this->Common_model->get_limit_megapolitan();
		$data['hukum'] = $this->Common_model->get_limit_hukum();
		$data['ekonomi'] = $this->Common_model->get_limit_ekonomi();
		$data['hiburan'] = $this->Common_model->get_limit_hiburan();
		$data['posts']=$this->Common_model->get_berita_by_kode($kode);
		$data['posts4']=$this->Common_model->get_berita_all();
		$this->load->view('V_listing',$data);	    
	}

	public function view()
	{
		$kode=$this->uri->segment(3);
		$kode2=$this->uri->segment(4);
		$this->Common_model->update_view($kode);
		$data['nusantara'] = $this->Common_model->get_limit_nusantara();
		$data['megapolitan'] = $this->Common_model->get_limit_megapolitan();
		$data['hukum'] = $this->Common_model->get_limit_hukum();
		$data['ekonomi'] = $this->Common_model->get_limit_ekonomi();
		$data['hiburan'] = $this->Common_model->get_limit_hiburan();		
		$data['posts']=$this->Common_model->get_berita_by_id($kode);
		$data['posts2']=$this->Common_model->get_berita_by_term_id($kode,$kode2);
		$data['posts3']=$this->Common_model->get_komen_by_id_post($kode);
		$data['posts4']=$this->Common_model->get_berita_all();
		$this->load->view('V_detail',$data);   
	}
	
	public function add_comment()
	{
		date_default_timezone_set("Asia/Jakarta");
		
		$kode=$this->uri->segment(3);
		$kode2=$this->uri->segment(4);
		$ip = $this->input->ip_address();
		
		$data = array(
			'newsId' => $_POST['post_id'],
			'author' => $_POST['name'],
			'email' => $_POST['email'],
            'content' => $_POST['comment'],
            'date' => date('Y-m-d H:i:s'),
            'ip' => $ip
        );
		$this->Common_model->insert($data, 'comment');
		$this->Common_model->update_comment($_POST['post_id']);
		redirect(base_url());
	}
	
	public function add_like($param)
	{		
		$this->Common_model->update_like($param);
		redirect(base_url());
	}
	
	public function content()
	{
		$kode=$this->uri->segment(3);
		$kode2=$this->uri->segment(4);
	    $data['nusantara'] = $this->Common_model->get_limit_nusantara();
		$data['megapolitan'] = $this->Common_model->get_limit_megapolitan();
		$data['hukum'] = $this->Common_model->get_limit_hukum();
		$data['ekonomi'] = $this->Common_model->get_limit_ekonomi();
		$data['hiburan'] = $this->Common_model->get_limit_hiburan();
		$data['content']=$this->Common_model->get_content($kode);
		$this->load->view('V_content',$data);   
	}
}