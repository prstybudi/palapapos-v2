<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct(){
        parent::__construct();
       $this->load->model('Common_model');
    }

	public function index()
	{
		$data = array();
		$data['nusantara'] = $this->Common_model->get_limit_nusantara();
		$data['megapolitan'] = $this->Common_model->get_limit_megapolitan();
		$data['hukum'] = $this->Common_model->get_limit_hukum();
		$data['ekonomi'] = $this->Common_model->get_limit_ekonomi();
		$data['hiburan'] = $this->Common_model->get_limit_hiburan();	
		$data['nusantaraslide'] = $this->Common_model->get_limit_nusantara5();
		$data['nasionalslide'] = $this->Common_model->get_limit_nasional1();
		$data['nasionalslide3'] = $this->Common_model->get_limit_nasional3();
		$data['nasionalslide4'] = $this->Common_model->get_limit_nasional4();
		$data['olahragaslide4'] = $this->Common_model->get_limit_olahraga4();
		$data['sumutslide4'] = $this->Common_model->get_limit_sumut4();
		$data['tapanuli4'] = $this->Common_model->get_limit_tapanuli4();
		$data['beritaterkini'] = $this->Common_model->get_last_news();
		$data['beritaterkini2'] = $this->Common_model->get_last_news2();
		$data['posts4']=$this->Common_model->get_berita_all();
		$data['nusantara2'] = $this->Common_model->get_limit_nusantara();
		$data['megapolitan2'] = $this->Common_model->get_limit_megapolitan();
		$data['hukum2'] = $this->Common_model->get_limit_hukum();
		$data['ekonomi2'] = $this->Common_model->get_limit_ekonomi();
		$data['politik2'] = $this->Common_model->get_limit_politik();
		$data['hiburan2'] = $this->Common_model->get_limit_hiburan();
		$data['widget1'] = $this->Common_model->widget_authors();
		$data['widget2'] = $this->Common_model->widget_comments();
		$data['widget3'] = $this->Common_model->widget_tag();
		$this->load->view('V_home',$data);
	}
}
