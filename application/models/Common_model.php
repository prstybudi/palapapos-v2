	<?php 
	class Common_model extends CI_Model {

		function get_last_news()
		{
			$hsl=$this->db->query("SELECT news.*,category.name,user.fullname FROM news 
									LEFT JOIN category ON news.categoryId=category.categoryId 
									LEFT JOIN user ON news.userId=user.userId
									ORDER BY news.date DESC LIMIT 24");
			$hsl = $hsl->result_array(); 
			return $hsl;
		}
		
		function get_last_news2()
		{
			$hsl=$this->db->query("SELECT news.*,category.name,user.fullname FROM news 
									LEFT JOIN category ON news.categoryId=category.categoryId 
									LEFT JOIN user ON news.userId=user.userId
									ORDER BY news.viewCount DESC LIMIT 12");
			$hsl = $hsl->result_array(); 
			return $hsl;
		}

		function get_berita_all()
		{
			$hsl=$this->db->query("SELECT news.*,category.name,user.fullname FROM news 
									LEFT JOIN category ON news.categoryId=category.categoryId 
									LEFT JOIN user ON news.userId=user.userId
									ORDER BY news.date DESC LIMIT 29");
			$hsl = $hsl->result_array(); 
			return $hsl;
		}
		
		function get_berita_by_kode($kode)
		{
			$hsl=$this->db->query("SELECT news.*,category.name,user.fullname FROM news 
									LEFT JOIN category ON news.categoryId=category.categoryId 
									LEFT JOIN user ON news.userId=user.userId
									WHERE category.categoryId='$kode'
									ORDER BY news.date DESC");
			$hsl = $hsl->result_array(); 
			return $hsl;
		}

		function get_berita_by_id($kode)
		{
			$hsl=$this->db->query("SELECT news.*,category.name,user.fullname FROM news 
									LEFT JOIN category ON news.categoryId=category.categoryId 
									LEFT JOIN user ON news.userId=user.userId
									WHERE news.newsId='$kode'
									ORDER BY news.date DESC");
			$hsl = $hsl->row(); 
			return $hsl;
		}

		function get_berita_by_term_id($kode,$kode2)
		{
			$hsl=$this->db->query("SELECT news.*,category.name,user.fullname FROM news 
									LEFT JOIN category ON news.categoryId=category.categoryId 
									LEFT JOIN user ON news.userId=user.userId
									WHERE category.categoryId='$kode2' AND news.newsId!='$kode'
									ORDER BY news.date DESC");
			$hsl = $hsl->result_array(); 
			return $hsl;
		}
		
		function get_komen_by_id_post($kode)
		{
			$hsl=$this->db->query("SELECT * FROM comment WHERE comment.newsId='$kode'");
			$hsl = $hsl->result_array(); 
			return $hsl;
		}
		function get_limit_nasional1()
		{
			$hsl=$this->db->query("SELECT news.*,category.name,user.fullname FROM news 
									LEFT JOIN category ON news.categoryId=category.categoryId 
									LEFT JOIN user ON news.userId=user.userId
									WHERE category.categoryId='2'
									ORDER BY news.date DESC LIMIT 1");
			$hsl = $hsl->row(); 
			return $hsl;
		}
		function get_limit_nasional4()
		{
			$hsl=$this->db->query("SELECT news.*,category.name,user.fullname FROM news 
									LEFT JOIN category ON news.categoryId=category.categoryId 
									LEFT JOIN user ON news.userId=user.userId
									WHERE category.categoryId='2'
									ORDER BY news.date DESC LIMIT 4");
			$hsl = $hsl->result_array(); 
			return $hsl;
		}
		function get_limit_olahraga4()
		{
			$hsl=$this->db->query("SELECT news.*,category.name,user.fullname FROM news 
									LEFT JOIN category ON news.categoryId=category.categoryId 
									LEFT JOIN user ON news.userId=user.userId
									WHERE category.categoryId='26'
									ORDER BY news.date DESC LIMIT 4");
			$hsl = $hsl->result_array(); 
			return $hsl;
		}
		function get_limit_sumut4()
		{
			$hsl=$this->db->query("SELECT news.*,category.name,user.fullname FROM news 
									LEFT JOIN category ON news.categoryId=category.categoryId 
									LEFT JOIN user ON news.userId=user.userId
									WHERE category.categoryId='4'
									ORDER BY news.date DESC LIMIT 4");
			$hsl = $hsl->result_array(); 
			return $hsl;
		}
		function get_limit_tapanuli4()
		{
			$hsl=$this->db->query("SELECT news.*,category.name,user.fullname FROM news 
									LEFT JOIN category ON news.categoryId=category.categoryId 
									LEFT JOIN user ON news.userId=user.userId
									WHERE category.categoryId='30'
									ORDER BY news.date DESC LIMIT 4");
			$hsl = $hsl->result_array(); 
			return $hsl;
		}
		function get_limit_nasional3()
		{
			$hsl=$this->db->query("SELECT news.*,category.name,user.fullname FROM news 
									LEFT JOIN category ON news.categoryId=category.categoryId 
									LEFT JOIN user ON news.userId=user.userId
									WHERE category.categoryId='2'
									ORDER BY news.date DESC LIMIT 3");
			$hsl = $hsl->result_array(); 
			return $hsl;
		}
		function get_limit_nusantara5()
		{
			$hsl=$this->db->query("SELECT news.*,category.name,user.fullname FROM news 
									LEFT JOIN category ON news.categoryId=category.categoryId 
									LEFT JOIN user ON news.userId=user.userId
									WHERE category.parent='3'
									ORDER BY news.date DESC LIMIT 5");
			$hsl = $hsl->result_array(); 
			return $hsl;
		}				
		function get_limit_nusantara()
		{
			$hsl=$this->db->query("SELECT news.*,category.name,user.fullname FROM news 
									LEFT JOIN category ON news.categoryId=category.categoryId 
									LEFT JOIN user ON news.userId=user.userId
									WHERE category.parent='3'
									ORDER BY news.date DESC LIMIT 4");
			$hsl = $hsl->result_array(); 
			return $hsl;
		}
		function get_limit_politik()
		{
			$hsl=$this->db->query("SELECT news.*,category.name,user.fullname FROM news 
									LEFT JOIN category ON news.categoryId=category.categoryId 
									LEFT JOIN user ON news.userId=user.userId
									WHERE category.categoryId='1'
									ORDER BY news.date DESC LIMIT 4");
			$hsl = $hsl->result_array(); 
			return $hsl;
		}
		function get_limit_megapolitan()
		{
			$hsl=$this->db->query("SELECT news.*,category.name,user.fullname FROM news 
									LEFT JOIN category ON news.categoryId=category.categoryId 
									LEFT JOIN user ON news.userId=user.userId
									WHERE category.parent='9'
									ORDER BY news.date DESC LIMIT 4");
			$hsl = $hsl->result_array(); 
			return $hsl;
		}
		function get_limit_hukum()
		{
			$hsl=$this->db->query("SELECT news.*,category.name,user.fullname FROM news 
									LEFT JOIN category ON news.categoryId=category.categoryId 
									LEFT JOIN user ON news.userId=user.userId
									WHERE category.parent='15'
									ORDER BY news.date DESC LIMIT 4");
			$hsl = $hsl->result_array(); 
			return $hsl;
		}
		function get_limit_ekonomi()
		{
			$hsl=$this->db->query("SELECT news.*,category.name,user.fullname FROM news 
									LEFT JOIN category ON news.categoryId=category.categoryId 
									LEFT JOIN user ON news.userId=user.userId
									WHERE category.parent='19'
									ORDER BY news.date DESC LIMIT 4");
			$hsl = $hsl->result_array(); 
			return $hsl;
		}
		function get_limit_hiburan()
		{
			$hsl=$this->db->query("SELECT news.*,category.name,user.fullname FROM news 
									LEFT JOIN category ON news.categoryId=category.categoryId 
									LEFT JOIN user ON news.userId=user.userId
									WHERE category.parent='22'
									ORDER BY news.date DESC LIMIT 4");
			$hsl = $hsl->result_array(); 
			return $hsl;
		}
		
		function widget_authors()
		{
			$hsl=$this->db->query("SELECT news.*,category.name,user.fullname FROM news 
									LEFT JOIN category ON news.categoryId=category.categoryId 
									LEFT JOIN user ON news.userId=user.userId
									ORDER BY news.date DESC LIMIT 5");
			$hsl = $hsl->result_array(); 
			return $hsl;
		}
		function widget_comments()
		{
			$hsl=$this->db->query("SELECT * FROM comment ORDER BY comment.date DESC LIMIT 5");
			$hsl = $hsl->result_array(); 
			return $hsl;
		}
		function widget_tag()
		{
			$hsl=$this->db->query("SELECT * FROM tags");
			$hsl = $hsl->result_array(); 
			return $hsl;
		}
		public function insert($data,$table){
			$this->db->insert($table,$data);        
			return $this->db->insert_id();
		}
		
		function update_view($id){
		//return current article
			$this->db->where('newsId', $id);
			$this->db->select('viewCount');
			$count = $this->db->get('news')->row();
		//then increase bt one
			$this->db->where('newsId', $id);
			$this->db->set('viewCount', ($count->viewCount + 1));
			$this->db->update('news');
		}
		
		function update_like($id){
		//return current article
			$this->db->where('newsId', $id);
			$this->db->select('likeCount');
			$count = $this->db->get('news')->row();
		//then increase bt one
			$this->db->where('newsId', $id);
			$this->db->set('likeCount', ($count->likeCount + 1));
			$this->db->update('news');
		}
		
		function update_comment($id){
		//return current article
			$this->db->where('newsId', $id);
			$this->db->select('commentCount');
			$count = $this->db->get('news')->row();
		//then increase bt one
			$this->db->where('newsId', $id);
			$this->db->set('commentCount', ($count->commentCount + 1));
			$this->db->update('news');
		}
		
		function get_content($kode)
		{
			$hsl=$this->db->query("SELECT * FROM content where contentId=$kode");
			$hsl = $hsl->row(); 
			return $hsl;
		}
	}
?>


